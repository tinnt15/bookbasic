import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { ngModuleJitUrl } from '@angular/compiler';
import { FormsModule } from '@angular/forms'

import { LoginComponent } from './login/login.component';
import { MainLayOutComponent } from './main-lay-out/main-lay-out.component';

const routesConfig: Routes = [
    {path: 'home', component: MainLayOutComponent},
    {path: 'login', component: LoginComponent},
    // {path: '',redirectTo: '/login', pathMatch:'full'},
    // {path: '**',redirectTo: '/login', pathMatch:'full'}
    { path: '**', redirectTo: '' }
  ];

  @NgModule({
      imports: [RouterModule.forRoot(routesConfig),
        FormsModule
    ],
      declarations: [
          LoginComponent,
          MainLayOutComponent
      ],
      exports: [RouterModule]
  })

  export class AppRoutingModule{}