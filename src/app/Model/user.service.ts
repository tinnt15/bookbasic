import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {AppConfig} from '../app.config'

import{ User } from './user.model'
@Injectable()
export class UserService {
  selectedUser: User;
  constructor(private http: Http,private config:AppConfig) { }

  login(InfUser : User){
    var body = JSON.stringify(InfUser);
    var headerOpetions = new Headers({ 'Content-Type' : 'application/Json'});
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers : headerOpetions });
    return this.http.post(this.config.apiUrl+'/api/login/checkuser',body, requestOptions).map(x => x.json());
  }
}
